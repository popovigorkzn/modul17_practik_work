﻿#include <iostream>
using namespace std;

class Vector {
private:
    double x;
    double y;
    double z;

public:
    Vector(double _x, double _y, double _z) {
        x = _x;
        y = _y;
        z = _z;
    }

    void print() {
        std::cout << "(" << x << ", " << y << ", " << z << ")" << std::endl;
    }

    double getLength() {
        return sqrt(x * x + y * y + z * z);
    }
};

int main() {
    Vector v(3.0, 4.0, 5.0);
    v.print();

    double length = v.getLength();
    std::cout << "Length: " << length << std::endl;

    return 0;
}